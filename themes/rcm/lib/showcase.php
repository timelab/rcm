<?php

function emitShowcases($cms)
{
	$html = "";

	$podparams = array('where' => 'synlig.meta_value=True', 'orderby' => 'ordning.meta_value ASC,id DESC', 'limit' => 4);
	$objectList = pods('puffar', $podparams);
	if ($objectList->total() > 0)
	{
		$count = 0;

        $html .= "<div class='container showcases PodsPuffar'>";
        $html .= "<div class='row showcase-row'>";
        while ($objectList->fetch())
        {
            $count++;
            $linkArr = $objectList->field('puff_lank');
            $linkTarget = '_self';
            if(!empty($linkArr)){
                $podpermalink = get_permalink($linkArr['ID']);
            }else{
                $podpermalink = $objectList->field('extern_lank');
                $linkTarget = "_blank";
            }

            $html .= "<a href='$podpermalink' target='$linkTarget'>";
            $html .= "<span class='col-md-4 col-sm-4 col-xs-12 showcase pos$count'>";
            $html .= "<span class='showcase-bg'>";
            $html .= "<img src='{$objectList->display('puff_bild')}' alt='{$objectList->field_title('label')}'/>";
            $html .= "</span>";

            $html .= "<span class='showcase-title'><p class='showcase-text'>{$objectList->field('post_title')}</p></span>";
            
            $html . "</span>";
            $html .= "</a>";
        }
        $html .= "</div>";
        $html .= "</div>";
	}
	return $html;
}

?>