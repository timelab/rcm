  <?php

    global $cms;

    $args = array(
      'orderby' => 'name',
      'parent' => 21
      );

    $categories = get_categories($args);
    $count = 0;

    if(!empty($categories)){
      $html = "";
      $prod_link = '';
      switch(count($categories)){
        case 1:
        case 2:
        case 3:
          $classes = "col-xs-4 col-xs-offset-4 ";
          break;
        case 4:
          $classes = "col-xs-6 col-sm-3 col-md-3";
          break;
        case 5:
        case 6:
          $classes = "col-xs-6 col-sm-2 col-md-2";
          break;
        default:
          $classes = "col-xs-6 col-sm-2 col-md-2";
      }
      foreach($categories as $category){

        $imgsrc = get_field('picture', $category->taxonomy . "_" . $category->term_id);
        $count++;

        $posts_array = get_posts(
            array(
                'posts_per_page' => 1,
                'tax_query' => array(
                    array(
                        'taxonomy' => 'category',
                        'field' => 'term_id',
                        'terms' => $category->term_id,
                    )
                )
            )
        );
      
      
          $prod_link = get_permalink($posts_array[0]->ID);
          $targetID = $posts_array[0]->ID;

          if(in_category( $category , get_post()->ID)){
            $linkClass = "class='activeCat'";

            $args = array(
            'orderby'      => 'title',
            'order'        => 'DESC',
            'post_type'    => 'post',
            'post__not_in' => array($post->ID),
            'category__in' => $category->cat_ID
            ); // post__not_in will exclude the post we are displaying
              $cat_posts = get_posts($args);
              $sibling_prod ='';
              $countCatPost = count($cat_posts);

              switch ($countCatPost) {
                case 1:
                  $sibprodGrid = "col-sm-3";
                  break;
                case 2:
                  $sibprodGrid = "col-sm-5";
                  $owlCarClass = 'owl-carousel';
                  break;
                default:
                  $sibprodGrid = "col-sm-9";
                  $owlCarClass = 'owl-carousel';
                  break;
              }

              $classNumbSib = "sib-".$countCatPost;

                foreach($cat_posts as $cat_post) {
                  $sib_link = get_permalink($cat_post->ID);

                  $sibling_prod .= "<a href='" . $sib_link . "'>";
                  $sibling_prod .=  "<div class='product-list'>";
                  $sibling_prod .=     "<div class='title'>";
                  $sibling_prod .=       "<h2>" . $cat_post->post_title . "</h2>";
                  $sibling_prod .=     "</div>";
                  $sibling_prod .=     "<div class='desc'>";
                  $sibling_prod .=        $cat_post->post_excerpt;
                  $sibling_prod .=     "</div>";
                  $sibling_prod .=  "</div>";
                  $sibling_prod .= "</a>";
                }
          }
          else{
            $linkClass = "";
          }

        $html .= "<a " . $linkClass . "href='". $prod_link ."' target_id='" . $targetID . "' target=''>";
        $html .= "<span class='{$classes} category pos{$count}'>";
        $html .= "<span class='category-image'>";
        $html .= "<img src='{$imgsrc}' alt='' />";
        $html .= "</span>";
        $html .= "<span class='category-title'>{$category->name}</span>";
        $html .= "</span>";
        $html .= "</a>";

        $manImgName = get_field('picture_text',$post->ID);
        if($manImgName != null){

          $manImgUrl = get_field('image',$post->ID);
          $manImgHtml = '<div class="manifactureimg">';
          $manImgHtml .=   '<a data-lightbox="image" href="' . $manImgUrl . '" target="_blank">‹‹ ' . $manImgName . ' ››</a>';
          $manImgHtml .= '</div>';
        }

      }
    }





  ?>

  <div class="wrap container mainText productpage" role="document">
    <div class="row">
        <div class="page-header text-center">
          <h1><?= __('Products', 'rcm') ?></h1>
        </div>
    </div>
    <div class="row">
      <div class="col-xs-12 categories">
          <?= $html ?>
      </div>
    </div>
    <div class="row product-slider <?= $classNumbSib ?>">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="col-sm-3 current">
         <div class="sel-product">
           <div class="title">
             <h2><?= the_title() ?></h2>
           </div>
           <div class="desc">
             <?= the_excerpt() ?>
           </div>
           <div class="current-text">
             <?= __('Product Specification', 'rcm') ?><br />
             <i class="fa fa-angle-down"></i>
             <img class="hidden-xs" src='<?= get_template_directory_uri() ?>/assets/images/blapil.png' />
           </div>
         </div>
        </div>
        <div class="<?= $sibprodGrid ?> slide">
          <div class="sib-prod <?= $owlCarClass ?>">
            <?= $sibling_prod ?>
          </div>
        </div>
      </div>
    </div>
    <div class="row product">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
          <div class="product">
            <div class="row prodPrev">
              <?php
        
                //var_dump($post->ID);
                if(has_post_thumbnail($post->ID)){
                  if(get_field('har_3d-bild',$post->ID)){
                    $ammount_pic = get_field('antal_bilder',$post->ID);
                    $thumbnail_url = get_the_post_thumbnail_url($post->ID);
                    $thumbnail_array = explode('/', $thumbnail_url);
                    $thumbnail_name = str_replace('-00.jpg', '', (end($thumbnail_array)));

                    echo "<a class='Magic360' href='#' data-magic360-options='filename: " . $thumbnail_name . "-{col}.jpg;columns:" . $ammount_pic . "; reverse-column: true;'><img src='" . get_the_post_thumbnail_url($post->ID) . "' alt='Produktbild' /></a>";
                  } else {
                    echo "<img src='" . get_the_post_thumbnail_url($post->ID, 'large') . "' alt='Produktbild' />";
                  }
        
                }
        
              ?>
            </div>
            <div class="entry-content">
              <h2><?php the_title(); echo " " . __('Specification', 'rcm') ?></h2>
              <div class="description">
                <?php the_content(); ?>
              </div>
              <?php
                $properties = get_field('property',$post->ID);
                $count = 0;
                $numbOfProp = (count($properties))-1;
        
                $html = "<div class='container-fluid properties'>";
        
                foreach($properties as $property){
                  if($count == 0){
                    $html .= "<div class='row property first'>";
                  }
                  else if($count == $numbOfProp){
                    $html .= "<div class='row property last'>";
                  }else{
                  $html .= "<div class='row property'>";
                  }
                  $html .= "<div class='col-xs-3 spec'>";
                  $html .= $property['rubrik'];
                  $html .= "</div>";
                  $html .= "<div class='col-xs-9 info'>";
                  $html .= $property['information'];
                  $html .= "</div>";
                  $html .= "</div>";
        
                  $count++;
                }
                $html .= "</div>";
        
                echo $html;
              ?>
              <?= $manImgHtml ?>
            </div>
          </div>
      </div>
    </div>
      <?php
        $brochure = get_option( 'brochure_lank_valj_sida' );
        $brochure_url = get_permalink( $brochure[0] );
      ?>
      <a class='orderLink' href="<?= $brochure_url ?>?id=<?= $post->ID ?>">
        <div class="row orderButton">
          <h2><?= __('Click here to order brochure', 'rcm') ?></h2>
        </div>
      </a>
  </div>

  <script>
    var themePath = "<?= get_template_directory_uri() ?>";
  </script>