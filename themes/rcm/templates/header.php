<header class="wrap container banner">
  <div class="headlogo">
    <a class="brand" href="<?= esc_url(home_url('/')); ?>"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/logo.png" alt="Logotype" class="topLogo"/></a>
  </div>
  <nav class="navbar navbar-default nav-primary">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
    </div>
    <div class="hidden-xs">
      <div class="navbar-collapse collapse">        
        <?php
        if (has_nav_menu('primary_navigation')) :
          wp_nav_menu(['walker' => new Roots_Nav_Walker(), 'theme_location' => 'primary_navigation', 'menu_class' => 'nav navbar-nav']);
        endif;
        ?>
      </div>
    </div>
    <div class="hidden-sm">
      <div class="navbar-collapse collapse">        
        <?php
        if (has_nav_menu('primary_navigation')) :
          wp_nav_menu(['walker' => new Roots_Nav_Walker_small(), 'theme_location' => 'primary_navigation', 'menu_class' => 'nav navbar-nav']);
        endif;
        ?>
      </div>
    </div>
  </nav>
</header>
