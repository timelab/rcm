<footer class="content-info">
  <div class="container">
  	<div class="col-xs-12 col-sm-3 copyright">
  		<strong><?php pll__('Copyright 2015 - RCM AB'); ?></strong>
  	</div>
  	<div class="nav bottom">
  		<nav class="nav-primary hidden-xs">
  		  <?php
  		  if (has_nav_menu('primary_navigation')) :
  		    wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'nav']);
  		  endif;
  		  ?>
  		</nav>
  	</div>
    <?php dynamic_sidebar('sidebar-footer'); ?>
  </div>
</footer>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-73145256-1', 'auto');
  ga('send', 'pageview');

</script>
