<head>
  <meta charset="utf-8">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0">
  <script type="text/javascript">
  	var baseUrl = '<?php echo get_template_directory_uri(); ?>';
  </script>
  <?php wp_head(); ?>

  <!-- Include js plugin -->
  <script src="https://maps.googleapis.com/maps/api/js?v=3.14&key=AIzaSyBJ9z8jhkuW6iYCt9R0G1wiOsZyNhgvA8Q&sensor=false" type="text/javascript"></script>
  <link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">
  <!-- Important Owl stylesheet -->
  <link rel="stylesheet" href="<?= get_template_directory_uri(); ?>/assets/styles/plugin/owl.carousel.css">
  <link rel="stylesheet" href="<?= get_template_directory_uri(); ?>/assets/styles/plugin/owl.theme.css">
  <!-- Important lightbox stylesheet -->
  <link rel="stylesheet" href="<?= get_template_directory_uri(); ?>/assets/styles/plugin/lightbox.min.css">


  <link href='https://fonts.googleapis.com/css?family=Roboto:100,100italic,400,400bold,700,300,300italic,500' rel='stylesheet' type='text/css'>
  
</head>
