<?php
/**
 * Template Name: contactpage
 */
?>

<?php
global $cms;

$active = 'active';
$facilities = $cms->getApi('Contact')->getFacilities();
$jscript = '';

foreach ($facilities as $facility)
{
	$address = $facility->getAddress();
	$exploded_address = explode(', ', ($address->getFormatted()));
	$country = end($exploded_address);

	$jscript .= $active != '' ? "var mapLat = ".$address->getLat()."; var mapLong = ".$address->getLong()."; " : "";

	$contact = "";

	$contact .= "<div class='adress'>";
	$contact .= "{$facility->getTitle()}";
	$contact .= "<br />{$address->getStreet()}";
	$contact .= "<br />{$address->getZip()} {$address->getCity()}";
	$contact .= "<br />" . $country;
	$contact .= "</div>";


	$mail 	  = '<div class="col-xs-12 col-sm-12 col-md-12">';
	$mail 	 .= '<h3>' . __('Send message', 'rcm') . '</h3>';
	$mail    .= do_shortcode('[contact-form-7 id="13" title="Kontaktformulär 1" ""]');
	$mail    .= '</div>';

	$staff= "";

	foreach ($facility->getPersonnel() as $person)
	{

		$staff .= '<div class="col-xs-12 col-md-4 col-sm-6 personal">';
		if ($person->getImage() !== null)
		{
			$staff .= "<img src='" . $person->getImage()->getSrc() . "' alt='{$person->getName()}' />";
		}
		else
		{
			$staff .= "<img src='" . get_template_directory_uri() . "/assets/img/profile_placeholder.png'  alt='{$person->getName()}' />";
		}

		$staff .= "<div class='PL_infosquare'>";
		$staff .= "<div class='PL_name'>{$person->getName()}</div>";
		$staff .= "<div class='PL_role'>{$person->getRole()}</div><br />";

		$pUppgifter = $person->getContactDetails();
		foreach ($pUppgifter as $pUppgift)
		{
			

			if ($pUppgift->getValue() !== '')
			{
				$staff .= "<div class='PL_Uppgift PL_{$pUppgift->getType()}'>";
				if ($pUppgift->getType() === 'email')
				{
					$staff .= "<a href='mailto:{$pUppgift->getValue()}'>{$pUppgift->getValue()}</a>";
				}
				else
				{
					$staff .= "<span class='key'></span>";
					$staff .= $pUppgift->getFormatted();
				}
				$staff .= "</div>";
			}
		
		}
		$staff .= "</div>";
		$staff .= "</div>";

		//var_dump($person->getImage()->getSrc());
	}
}
?>


<div class="wrap container mainText contactpage" role="document" data-contactpage-text="<?php echo get_field("contact_page_text", "option"); ?>">
	<div class="row">
		<?php get_template_part('templates/page', 'header'); ?>
	</div>
	<div class="row content-row">
			<div class="content">
		<div class="row">
			<script type="text/javascript">
				var responsiveMap;
				var beachMarker;
				var mapOptions;
				<?php echo $jscript; ?>
			</script>

			<div id="map-canvas" class="col-sm-12"></div>
			<div class="col-sm-12">
			    <div class="mapBottom"></div>
			</div>
		</div>
		<div class="row">
			<div class="facilitys contact">
				<div class="col-sm-6 facility">
					<h3><?= __('Location', 'rcm') ?></h3>
					<h4><?= __('Visiting address', 'rcm') ?></h4>
					<?= $contact ?>
				</div>
				<div class="col-sm-6 facility billing">
					<h4><?= __('Billing address', 'rcm') ?></h4>
					<?= get_field( "CompanyName" ) ?><br/>
					<?= get_field( "box" ) ?><br/>
					<?= get_field( "post_number" ) ?><br/>
					<?= get_field( "country" ) ?>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="mailForm contact">
				<?= $mail ?>
			</div>
		</div>
		<div class="row">
				<div class="employe contact">					
					<h3><?= __('Employees', 'rcm') ?></h3>
					<div><?= $staff ?></div>
				</div>
			</div>
		</div>
	</div>
</div>