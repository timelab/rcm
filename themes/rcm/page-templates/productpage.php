<?php
	/**
	 * Template Name: Productpage
	 */

	global $cms;

	$args = array(
	  'orderby' => 'name',
	  'parent' => 21
	  );

	$categories = get_categories($args);
	$count = 0;

	if(!empty($categories)){
		$html = "";
		$prod_link = '';
		switch(count($categories)){
			case 1:
			case 2:
			case 3:
				$classes = "col-xs-4 col-xs-offset-4 ";
				break;
			case 4:
				$classes = "col-xs-6 col-sm-3 col-md-3";
				break;
			case 5:
			case 6:
				$classes = "col-xs-6 col-sm-2 col-md-2";
				break;
			default:
				$classes = "col-xs-6 col-sm-2 col-md-2";
		}
		foreach($categories as $category){

			$imgsrc = get_field('picture', $category->taxonomy . "_" . $category->term_id);
			$count++;

			$posts_array = get_posts(
			    array(
			        'posts_per_page' => 1,
			        'tax_query' => array(
			            array(
			                'taxonomy' => 'category',
			                'field' => 'term_id',
			                'terms' => $category->term_id,
			            )
			        )
			    )
			);
    
    
    		$prod_link = get_permalink($posts_array[0]->ID);
    		$targetID = $posts_array[0]->ID;
   

			$html .= "<a href='". $prod_link ."' target_id='" . $targetID . "' target=''>";
			$html .= "<span class='{$classes} category pos{$count}'>";
			$html .= "<span class='category-image'>";
			$html .= "<img src='{$imgsrc}' alt='' />";
			$html .= "</span>";
			$html .= "<span class='category-title'>{$category->name}</span>";
			$html .= "</span>";
			$html .= "</a>";
		}
	}





?>

<div class="wrap container mainText productpage noBG" role="document">
	<div class="row">
			<?php get_template_part('templates/page', 'header'); ?>
	</div>
	<div class="row">
		<div class="col-xs-12 categories">
				<?= $html ?>
		</div>
	</div>
	<div class="row fill">
	</div>
</div>