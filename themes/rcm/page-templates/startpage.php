<?php
/**
 * Template Name: Startsida
 */

global $cms;
?>

<div class="container-fluid slider">
  <div class="row">
		<?php putRevSlider('sliderStartpage', 'homepage'); ?>
  </div>
</div>   

<div class="mainText startpage" role="document">
	<div class="container-fluid text-center">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<div class="page-text">
				<?php get_template_part('templates/content', 'page'); ?>
			</div>
		</div>
	</div>
</div>

<?= emitShowcases($cms); ?>
