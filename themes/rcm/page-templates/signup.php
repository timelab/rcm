<?php
/**
 * Template Name: signup
 */

?>

<div class="wrap container mainText signup" role="document" data-newsletter-text="<?php echo get_field("newsletter_text", "option"); ?>" data-contactme-text="<?php echo get_field("contact_me_text", "option"); ?>">

	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<div class="row">
				<?php get_template_part('templates/page', 'header'); ?>
			</div>
			<div class="page-text">
				<div class="row">
					<div class="col-sm-6 newsletter">
						<h3><?= __('Sign up and get our newsletter', 'rcm') ?></h3>
						<?= do_shortcode('[mc4wp_form id="467"]');?>
					</div>
					<div class="col-sm-6 contactreq">
						<h3><?= __('I would like RCM Global Markets to contact me.', 'rcm') ?></h3>
						<?= do_shortcode('[contact-form-7 id="133" title="We contact you"]');?>	
					</div>
				</div>
			</div>
		</div>
	</div>
</div>