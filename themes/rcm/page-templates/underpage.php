<?php
/**
 * Template Name: underpage
 */

?>

<div class="wrap container mainText undersida" role="document">
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<div class="row">
				<?php get_template_part('templates/page', 'header'); ?>
			</div>
			<div class="page-text">
				<?php get_template_part('templates/content', 'page'); ?>
			</div>
		</div>
	</div>
</div>