<?php
/**
 * Template Name: brochure
 */

	$post = get_post();
	if(isset($_GET['id'])){
		$preSel = $_GET['id'];
	}

	$args = array(
	  'orderby' => 'name',
	  'parent' => 21
	  );

	$html_checkboxes .= "";

	$categories = get_categories($args);
	foreach($categories as $category){

		$posts_array = get_posts(
		    array(
		        'tax_query' => array(
		            array(
		                'taxonomy' => 'category',
		                'field' => 'term_id',
		                'terms' => $category->term_id,
		            )
		        )
		    )
		);

		foreach ($posts_array as $post_prod) { 
			$selected =  ($post_prod->ID == $preSel) ? "checked" : '';
			$imgUrl = (get_the_post_thumbnail_url($post_prod->ID, 'medium') != null) ? get_the_post_thumbnail_url($post_prod->ID, 'medium') : "http://placehold.it/300x138";
			$html_checkboxes .= "<div class='col-xs-4 brochure-item'>";
			$html_checkboxes .= "<img src='" . $imgUrl . "' alt='Produktbild' />";
			$html_checkboxes .= "<div class='prod-checkbox'><input type='checkbox' name='" . $post_prod->post_title ."' value='Product' " . $selected . "> <span>" . $post_prod->post_title . "</span></div><br>";
			$html_checkboxes .= "</div>";
		}
	}

?>

<div class="wrap container mainText brochure" role="document">
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<div class="row">
				<?php get_template_part('templates/page', 'header'); ?>
			</div>
			<div class="page-text">
				<div class="row product-list">
					<div class="col-sm-8 col-sm-offset-2">
						<?= $html_checkboxes ?>
					</div>
				</div>
				<div class="row prodreq-form">
					<div class="col-sm-8 col-sm-offset-2">
						<?= do_shortcode( '[contact-form-7 id="239" title="order brochure"]' ) ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>