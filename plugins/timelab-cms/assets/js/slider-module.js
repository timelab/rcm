function update_list_order(items, order_input_name) {
    items.each(function (index) {
        $(items[index]).find($('input[name*=' + order_input_name + ']')).val(index);
    });
}

var original_remove_button_contents;
function remove_image($button) {
    var $slider_image = $button.closest('.slider-image-panel');
    var $input = $slider_image.find('input[name*="image_remove"]');

    $input.val('true').trigger('change');

    var $all_inputs = $slider_image.find('input[type!=hidden], select, textarea');

    if (!$slider_image.hasClass('removed')) {
        $slider_image.addClass('removed');

        original_remove_button_contents = $button.html();
        $button.html('<span class="glyphicon glyphicon-trash"></span> Ångra Ta bort');
        $input.val('true').trigger('change');
    } else {
        $slider_image.removeClass('removed');
        $button.html(original_remove_button_contents);
        $input.val('').trigger('change');
    }
}

/**
 * Bildspel
 */
$(function() {

    $("#slider-image-list").sortable({
        forcePlaceholderSize: true,
        placeholder: 'placeholder',
        opacity: 0.8,
        update: function() {
            update_list_order($('#slider-image-list li'), 'image_order');
        }
    });

    $( "#sortable" ).disableSelection();

    function add_image(image) {
        var order = ($('#slider-image-list input[name*="image_order"]').length <= 0) ? 0 : parseFloat($('#slider-image-list input[name*="image_order"]').last().val()) + 1;

        $('#slider-image-list').append(
            // Hämta templaten och ersätt placeholders med data
            $('#slider-image-template').html().format(image.id, image.url, order)
        );

        $('#slider-image-list .remove-image').unbind('click');
        $('#slider-image-list .remove-image').click(function(e) {
            remove_image($(this));
            e.preventDefault();
        });
    }

    var upload_frame = create_media_frame("Lägg in bild", "Lägg till", add_image, true);

    $('#new-image').click(function() {
        upload_frame.close($(this));
        upload_frame.open($(this));
    });

    // Ta bort
    $('#slider-image-list .remove-image').click(function(e) {
        remove_image($(this));
        e.preventDefault();
    });

    $('.input-daterange').datepicker({
        language: "sv"
    });

});