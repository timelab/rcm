<?php

// Moduler
require_once('dashboard.php');
require_once('slider.php');
require_once('brand_editor.php');
require_once('gallery_editor.php');



/**
 * Class timelab_admin
 * Modiferar wordpress admin gränsnittet.
 */
class admin extends cms_module {

    /**
     * Detta innehåller slider objektet
     * @var slider
     */
    public $slider;

    /**
     * Detta är dashboarden
     * @var dashboard
     */
    public $dashboard;

    /**
     * Detta är bildgalleri hanteraren
     * @var gallery_editor
     */
    public $gallery;

    /**
     * Konstruktorn för hela pluginet, denna funktion initialiserar allting.
     */
    public function __construct() {
        parent::__construct();

        add_action( 'login_enqueue_scripts', array($this, 'add_admin_scripts') );

        if ( version_compare( get_bloginfo( 'version' ), '4.9', '>=' ) ) {
            add_action( 'admin_enqueue_scripts', array($this, 'add_admin_scripts') );
        }else{
            add_action( 'admin_enqueue_scripts', array($this, 'add_admin_scripts_pre') );
        }

        //add_action( 'admin_bar_menu', array($this, 'change_admin_bar_menu'), 999 );
        //add_action( 'in_admin_header', array($this, 'add_user_bar'), 0);
        //add_action( 'admin_menu', array($this, 'change_admin_menu'));
        add_filter( 'admin_footer_text', array($this, 'change_admin_footer') );
        add_filter( 'admin_title', array($this, 'change_title'), 10, 2);
        add_filter( 'gettext', array($this, 'rename_admin_menu_items') );
        //add_filter( 'show_admin_bar', '__return_false' );

        add_action( 'save_post', array($this, 'on_save_post'));

        add_filter( 'admin_body_class', array($this, 'add_role_body_class'));

        // Initialisera samtliga moduler
        $this->dashboard = new dashboard($this);
        $this->gallery = new gallery_editor($this);
    }

    /**
     * Ändrar <title> i adminet
     * @param $admin_title
     * @param $title
     * @return string
     */
    function change_title($admin_title, $title) {
        return get_bloginfo('name') . ' - ' . $title;
    }

    /**
     * Döper om menyalternativ i adminet
     * @param $menu
     * @return mixed
     */
    function rename_admin_menu_items($menu) {
        $menu = str_ireplace( 'Adminpanel', 'Nyheter', $menu );
        return $menu;
    }

    /**
     * Lägger till den nya adminbaren
     */
    function add_user_bar() {

        $username = wp_get_current_user()->display_name;
        $role = $this->is_admin() ? "(Administratör)" : "";
        $site_url = site_url();
        $logout_url = wp_logout_url();
        $warning = null;
        $warning_url = null;
        if ($this->is_admin()) {
            $warning = get_option('blog_public') ? '' : 'Sidan indexeras <strong>INTE</strong> av sökmotorer i nuläget';
            $warning_url = admin_url('options-reading.php');
        }

        echo "
        <div class='user-box'>
            <div id='wp-admin-bar-menu-toggle' class='hidden-lg hidden-md hidden-sm'><a class='ab-item' href='#' aria-expanded='true'><span class='ab-icon'></span><span class='screen-reader-text'>Meny</span></a></div>

            <div class='user-box-info'>
                Inloggad som
                <a href='profile.php' class='username'>$username</a>
                <span class='role'>$role</span>
                <span class='label label-danger'><a style='color: white' href='{$warning_url}'>$warning</a></span>
            </div>
            <ul class='user-box-menu'>
                <li>
                    <a href='$site_url'>Gå till hemsida</a>
                </li>
                <li>
                    <a href='$logout_url'>Logga ut</a>
                </li>
            </ul>
            <div class='clear'></div>
        </div>";
    }

    /**
     * Lägger till admin stylesheet och alla skript i <HEAD> 
     * Verision 4.8.4 och mindre
     */
    function add_admin_scripts_pre() {

        // Lägg in admin css
        $css_url = plugins_url() . "/timelab-cms/assets/css/admin.min.css";
        echo "<link rel='stylesheet' type='text/css' href='{$css_url}'>";

        // Lägg in användare css (Om inte admin)
        if (!$this->is_admin()) {
            $css_url = plugins_url() . "/timelab-cms/assets/css/user.min.css";
            echo "<link rel='stylesheet' type='text/css' href='{$css_url}'>";
        }

        // Lägg in javascript
        wp_enqueue_media();
        echo sprintf('<script type="text/javascript" src="%s"></script>', plugins_url() . "/timelab-cms/assets/js/scripts.min.js" );
        echo '<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyBZSh82BOfJHnzFTkhsO44bslos5wV8Wqg&sensor=false"></script>';

        // Lägg in kundens logo
        $customer_logo = get_option('customer_logo');

        if (!empty($customer_logo)) {
            echo "<style type='text/css'> body.wp-core-ui #login h1 a { background-image: url('{$customer_logo}') } </style>";
        }

        // Lägg till override css om det finns i nuvarande tema
        if (file_exists( get_template_directory() . "/timelab_cms_custom.css" )) {
            echo "<link rel='stylesheet' type='text/css' href='" . get_template_directory_uri() . "/timelab_cms_custom.css'>";
        }

        // Lägg till override js om det finns i nuvarande tema
        if (file_exists( get_template_directory() . "/timelab_cms_custom.js" )) {
            echo "<script src='" . get_template_directory_uri() . "/timelab_cms_custom.js'></script>";
        }
    }

    /**
     * Lägger till admin stylesheet och alla skript i <HEAD>
     */
    function add_admin_scripts() {
        if ( is_admin() && preg_match("/timelab_cms/", get_current_screen()->base) ){
            // Lägg in admin css
            $css_url = plugins_url() . "/timelab-cms/assets/css/admin.min.css";
            echo "<link rel='stylesheet' type='text/css' href='{$css_url}'>";

            // Lägg in användare css (Om inte admin)
            if (!$this->is_admin()) {
                $css_url = plugins_url() . "/timelab-cms/assets/css/user.min.css";
                echo "<link rel='stylesheet' type='text/css' href='{$css_url}'>";
            }

            // Lägg in javascript
            wp_enqueue_media();

            wp_deregister_script( 'jquery-ui-core' );
            wp_enqueue_script('jquery-ui-core', plugins_url() . "/timelab-cms/assets/js/plugins/jquery/jquery-ui-1.10.4.custom.js", array('jquery'), '1.10.4', 1 );

            //wp_deregister_script( 'jquery' );
            wp_register_script('default-admin', plugins_url() . "/timelab-cms/assets/js/scripts.min.js");
            wp_register_script('default-admin', 'https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyBZSh82BOfJHnzFTkhsO44bslos5wV8Wqg&sensor=false');
            wp_enqueue_script('default-admin');

            wp_register_script('mediaelement', plugins_url('wp-mediaelement.min.js', __FILE__), array('jquery'), '4.8.2', true);
            wp_enqueue_script('mediaelement');


            // Lägg in kundens logo
            $customer_logo = get_option('customer_logo');

            if (!empty($customer_logo)) {
                echo "<style type='text/css'> body.wp-core-ui #login h1 a { background-image: url('{$customer_logo}') } </style>";
            }

            // Lägg till override css om det finns i nuvarande tema
            if (file_exists( get_template_directory() . "/timelab_cms_custom.css" )) {
                echo "<link rel='stylesheet' type='text/css' href='" . get_template_directory_uri() . "/timelab_cms_custom.css'>";
            }

            // Lägg till override js om det finns i nuvarande tema
            if (file_exists( get_template_directory() . "/timelab_cms_custom.js" )) {
                wp_register_script('custom-cms-admin', get_template_directory_uri() . "/timelab_cms_custom.js");
                wp_enqueue_script('custom-cms-admin');
            }
        }
    }

    /**
     * Modifierar admin menyn
     */
    function change_admin_menu() {
        // Om användaren inte är admin, rensa bort en massa alternativ
        if (!$this->is_admin()) {
            remove_menu_page('link-manager.php');
            remove_menu_page('tools.php');
            remove_menu_page('edit-comments.php');
            remove_menu_page('upload.php');
            remove_menu_page('edit.php');
            remove_menu_page('edit.php?post_type=page');
            remove_menu_page('profile.php');
        }
    }

    /**
     * Utför ändringar på footern i administrationen, lägger bland annat till Timelab branding
     * @return string
     */
    function change_admin_footer() {
        echo '';
    }

    /**
     * Utför ändringar på admin bar i administrationen
     * @param $wp_admin_bar Skickas in av wordpress genom add_action
     */
    function change_admin_bar_menu( $wp_admin_bar ) {
        foreach ($wp_admin_bar->get_nodes() as $node ) {
            if ($node->id === 'wp-logo' || $node->id === 'new-content' || $node->id === 'comments') {
                $wp_admin_bar->remove_node($node->id);
            }
        }
    }

    /**
     * Hämtar ut kundens logga.
     * @return string Länken till kundens logo
     */
    function get_customer_logo() {
        return get_option('customer_logo');
    }

    /**
     * Lägger till rollen som klass i admin
     * @param $classes
     * @return string
     */
    public function add_role_body_class($classes) {
        $classes .= " ";
        $classes .= ($this->is_admin()) ? "role-admin" : "role-user";

        return $classes;
    }

    public function on_save_post() {
        //$this->clear_cache();
    }

}