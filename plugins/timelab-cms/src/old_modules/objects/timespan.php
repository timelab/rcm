<?php

class timespan {

    /**
     * Den unika identifiern för tiden
     * @var int
     */
    public $ID;

    /**
     * Tidens titel (Tex: Måndag)
     * @var string
     */
    public $title;

    /**
     * Tid från (Tex: 08:00)
     * @var string
     */
    public $from;

    /**
     * Tid till (Tex: 16:00)
     * @var string
     */
    public $to;

    /**
     * Ordningen öppettiden har i listan
     * @var int
     */
    public $order;

    /**
     * Skapa en ny timespan
     * @param int $id
     * @param string $title
     * @param string $to
     * @param string $from
     * @param int $order
     */
    public function __construct($id, $title, $from, $to, $order) {
        $this->ID = $id;
        $this->title = $title;
        $this->from = $from;
        $this->to = $to;
        $this->order = $order;
    }

    public function __toString() {
        return $this->from . " - " . $this->to;
    }


} 