<?php

/**
 * Detta är en gatuadress
 * Class address
 */
class address {

    /**
     * @var int Den unika identifiern för addressen
     */
    public $ID;

    /**
     * @var string Titeln för addressen
     */
    public $title;

    /**
     * @var string Den formatterade gatuadressen. Tex "Kungsgatan 34A, 972 41 Luleå, Sverige"
     */
    public $formatted;

    /**
     * @var string Gatan. Tex "Kungsgatan 34A"
     */
    public $street;

    /**
     * @var string Den oformatterade postkoden. Tex "97241"
     */
    public $zip;

    /**
     * @var string Staden för adressen. Tex 'Luleå'
     */
    public $city;

    /**
     * @var float Latitud koordinaten för adressen. Tex '65.58672899999999'
     */
    public $lat;

    /**
     * @var float Longitud koordinaten för adressen. Tex '22.155877'
     */
    public $long;

    /**
     * @var int Ordnigen som adressen ska komma i. 0 är först
     */
    public $order;

    /**
     * @param $ID
     * @param $title
     * @param $formatted
     * @param $street
     * @param $zip
     * @param $city
     * @param $lat
     * @param $long
     * @param $order
     */
    public function __construct($ID, $title, $formatted, $street, $zip, $city, $lat, $long, $order) {
        $this->ID = $ID;
        $this->title = $title;
        $this->formatted = $formatted;
        $this->street = $street;
        $this->zip = $zip;
        $this->city = $city;
        $this->lat = floatval($lat);
        $this->long = floatval($long);
        $this->order = $order;
    }

    public function __toString() {
        return $this->formatted;
    }
} 