<?php
/**
 * Class slider
 */
class slider extends cms_module {

    /**
     * Namnet på bildspels posttypen
     * @var string
     */
    var $slider_post_type_name = "timelab_cms_slider";

    /**
     * Namnet på bildspels bild posttypen
     * @var string
     */
    var $image_post_type_name = "timelab_cms_image";

    /**
     * Konstruktorn, hookar in våra funktioner in i Wordpress
     * @param dashboard $cms Dashboard objektet, används för att skapa snabblänk på dashboardet
     */
    public function __construct($cms) {
        add_action('admin_init', array($this, 'create_post_types'));
        add_action('admin_menu', array($this, 'create_slider_list'));

        $cms->dashboard->add_dashboard_quick_button('Bildspel', 'Lägg till och ta bort bilder från dina bildspel', admin_url('admin.php?page=timelab_cms_sliders'), 'glyphicon-camera');
    }

    /**
     * Skapar post type för både bildspel och bildspelsbilder
     */
    function create_post_types() {
        // Post types
        $post_type_args =
            array(
                'labels' => array(
                    'name' => "Bildspel"
                ),
                'description' => "Detta är ett bildspel",
                'public' => true,
                'exclude_from_search' => true,
                'supports' => array('title', 'revisions', 'custom-fields')
            );

        register_post_type($this->slider_post_type_name, $post_type_args);

        $post_type_args =
            array(
                'labels' => array(
                    'name' => "Bild"
                ),
                'description' => "Detta är en bild",
                'public' => true,
                'exclude_from_search' => true,
                'supports' => array('revisions', 'custom-fields')
            );

        register_post_type($this->image_post_type_name, $post_type_args);
    }

    /**
     * Routar anropet till rätt funktion med hjälp av $_GET['action'] (Tex 'edit')
     */
    function create_slider_list() {

        $page_function = false;
        $page_title = '';

        // Välj vilken sida som ska renderas beroende på $_GET['action']
        $action = (isset($_GET['action']) && isset($_GET['page']) && $_GET['page'] == 'timelab_cms_sliders') ? $_GET['action'] : "";

        switch ($action) {
            case 'edit':
                $page_function = 'output_slider_editor';
                $page_title = 'Redigera Bildspel';
                break;
            case 'save':
                // Spara slidern, skriv sedan ut slider editorn
                $this->save_slider();
                $page_function = 'output_slider_editor';
                $page_title = 'Redigera Bildspel';
                break;
            case 'delete':
                if ($this->is_admin()) {
                    // Ta bort slidern, skriv sedan ut slider listan
                    $this->delete_slider();
                    $page_function = 'output_slider_list';
                    $page_title = 'Bildspel';
                } else {
                    wp_die("Inte behörighet för att utföra detta");
                }
                break;
            default:
                // Om användaren bara har ett bildspel, redirecta då till detta då kunden kommer till bildspelslistningen
                if (isset($_GET['page']) && $_GET['page'] == 'timelab_cms_sliders') {
                    $sliders = get_posts(
                        array(
                            'post_type' => $this->slider_post_type_name
                        )
                    );

                    if (!$this->is_admin() && count($sliders) == 1) {
                        wp_redirect(admin_url('admin.php?page=timelab_cms_sliders&post=' . $sliders[0]->ID . '&action=edit'));
                    }
                }

                // Skriv ut bildspel listan
                $page_function = 'output_slider_list';
                $page_title = 'Bildspel';
                break;
        }

        add_menu_page( $page_title, 'Bildspel', 'edit_pages', 'timelab_cms_sliders', array($this, $page_function), 'dashicons-format-gallery', 16.1 );
    }

    /**
     * Skriver ut listan med bildspel
     */
    function output_slider_list() {
        $this->admin_page_template_top();

        if ($this->is_admin()) {
            $create_url = admin_url('post-new.php?post_type=timelab_cms_slider');
            echo "<a class='btn btn-success' href='$create_url'><i class='glyphicon glyphicon-plus'></i> Lägg till nytt Bildspel</a>";
        }

        $sliders = get_posts(
            array(
                'post_type' => $this->slider_post_type_name,
                'posts_per_page' => -1
            )
        );

        echo "<ul>";
        foreach ($sliders as $slider) {
            $this->output_slider_puff($slider);
        }
        echo "</ul>";

        $this->admin_page_template_bottom();
    }

    /**
     * Skriver ut editorn för ett bildspel (Angivet i $_GET['post'])
     */
    function output_slider_editor() {
        // Kolla om bildspelet existerar
        if (isset($_GET['post']) && get_post_type($_GET['post']) != $this->slider_post_type_name) {
            wp_die('Det angivna bildspelet existerar inte.');
        }

        $this->admin_page_template_top();

        $slider = get_post($_GET['post']);

        $action_url = admin_url('admin.php?page=timelab_cms_sliders&post='. $slider->ID .'&action=save');
        echo "<form action='$action_url' method='post'>";
        echo "<div class='row'>";

        if ($this->is_admin()) {
            echo "<div class='col-sm-9'>";
            echo "<input name='title' type='text' class='form-control input-lg' value='{$slider->post_title}'></h2>";
            echo "</div>";
        }
        echo "
        <div class='col-sm-9'>
        <h2>Bilder</h2>
        <ul class='panel-group' id='slider-image-list'>";


        $images = get_posts(
            array(
                'post_type' => 'timelab_cms_image',
                'posts_per_page' => -1,
                'post_parent' => $_GET['post'],
                'meta_key' => 'order',
                'orderby' => 'meta_value_num',
                'order' => 'ASC'
            )
        );

        foreach ($images as $image) {
            $this->output_slider_image($image->ID);
        }

        echo "</ul></div><div class='col-sm-3 sidemenu'>";
        echo "<a href='#' class='btn btn-block btn-lg btn-success' id='new-image'><span class='glyphicon glyphicon-plus'></span> Lägg till ny bild</a>";
        echo "<button target='publish' class='btn btn-block btn-lg btn-success'><span class='glyphicon glyphicon-ok'></span> Spara ändringar</button>";
        if ($this->is_admin()) {
            $trash_url = admin_url('admin.php?page=timelab_cms_sliders&post=' . $_GET['post'] . '&action=delete');
            echo "
            <a href='$trash_url' class='btn btn-block btn-lg btn-danger' data-confirm='true' data-confirm-title='Ta bort bildspel' data-confirm-text='Är du säker på att du vill ta bort bildspelet samt alla bilder?'>
            <span class='glyphicon glyphicon-trash'></span> Ta bort bildspel</a>";

            $enable_title_checked = (get_post_meta($_GET['post'], 'enable_title', true) == 1) ? 'checked' : '';
            $enable_subtitle_checked = (get_post_meta($_GET['post'], 'enable_subtitle', true) == 1) ? 'checked' : '';

            echo "
            <h3>Bildspelet ska ha:</h3>
            <label for='enable_title'>Rubrik</label>
            <input type='checkbox' name='enable_title' value='1' $enable_title_checked></input>
            <label for='enable_subtitle'>Underrubrik</label>
            <input type='checkbox' name='enable_subtitle' value='1' $enable_subtitle_checked></input>
            ";
        }

        echo "<input type='submit' class='hidden' id='publish'></input>";
        echo "</div>";

        echo "</div></form>";

        echo "<div id='slider-image-template' class='hidden'>";
        $this->output_slider_image();
        echo "</div>";

        $this->admin_page_template_bottom();
    }

    /**
     * Sparar den nuvarande bildspelet samt ändringar till alla tillhörande bilder
     */
    function save_slider() {
        if ($_GET['action'] == 'save' && current_user_can('edit_pages') && isset($_GET['post']) && get_post_type($_GET['post']) == $this->slider_post_type_name) {
            // Spara ändringar till bildspelet
            $post_changes = array( 'ID' => $_GET['post']);

            if ($this->is_admin()) {
                if (isset($_POST['title'])) {
                    $post_changes['post_title'] = $_POST['title'];
                }

                if (isset($_POST['enable_title']) && $_POST['enable_title'] == 1) {
                    update_post_meta($_GET['post'], 'enable_title', 1);
                } else {
                    update_post_meta($_GET['post'], 'enable_title', 0);
                }

                if (isset($_POST['enable_subtitle']) && $_POST['enable_subtitle'] == 1) {
                    update_post_meta($_GET['post'], 'enable_subtitle', 1);
                } else {
                    update_post_meta($_GET['post'], 'enable_subtitle', 0);
                }
            }

            wp_update_post($post_changes);

            // Spara bilder
            if (isset($_POST['image_id'])) {

                foreach ($_POST['image_id'] as $image_index=>$image_id) {

                    // Ta bort bilder som är flaggad för borttagning
                    if (strtolower($_POST['image_remove'][$image_index]) == 'true') {

                        if (!empty($_POST['image_id'])) {
                            wp_delete_post($_POST['image_id'][$image_index]);
                        }

                        continue;
                    }

                    $image = array(
                        "id"        => $image_id,
                        "text"      => $_POST['image_text'][$image_index],
                        "title"     => $_POST['image_title'][$image_index],
                        "subtitle"  => $_POST['image_subtitle'][$image_index],
                        "media_id"  => $_POST['media_id'][$image_index],
                        "order"     => $_POST['image_order'][$image_index],
                        "href"      => $_POST['image_href'][$image_index],
                        "target"    => $_POST['image_target'][$image_index],
                        "date_from" => $_POST['image_date_from'][$image_index],
                        "date_to"   => $_POST['image_date_to'][$image_index]
                    );

                    $image_data = array(
                        'post_type' => $this->image_post_type_name,
                        'post_parent' => $_GET['post'],
                        'post_status' => 'publish',
                        'post_title' => $_POST['image_title'][$image_index]
                    );

                    if (empty($image['id'])) {
                        $image['id'] = wp_insert_post($image_data);
                    } else {
                        wp_update_post(array_merge($image_data, array(
                            "ID" => $image['id']
                        )));
                    }

                    // Uppdatera meta data på iblden
                    update_post_meta($image['id'], 'text', $image['text']);
                    update_post_meta($image['id'], 'subtitle', $image['subtitle']);
                    update_post_meta($image['id'], 'media_id', $image['media_id']);
                    update_post_meta($image['id'], 'order', $image['order']);
                    update_post_meta($image['id'], 'href', $image['href']);
                    update_post_meta($image['id'], 'target', $image['target']);
                    update_post_meta($image['id'], 'date_from', $image['date_from']);
                    update_post_meta($image['id'], 'date_to', $image['date_to']);
                }


            }

            // Rensa cache
            $this->clear_cache();

            // Redirecta tillbaka till editorn
            wp_redirect(admin_url('admin.php?page=timelab_cms_sliders&post=' . $_GET['post'] . '&action=edit&message=save_success'));
        }
    }

    /**
     * Tar bort nuvarande slider samt alla bilder. Detta går inte att ångra.
     */
    function delete_slider() {
        if (isset($_GET['post']) && get_post_type($_GET['post']) == $this->slider_post_type_name) {

            // Hämtar ut alla bilder som tillhör bildspelet
            $images = get_posts(
                array(
                    'post_type' => 'timelab_cms_image',
                    'posts_per_page' => -1,
                    'post_parent' => $_GET['post'],
                    'meta_key' => 'order',
                    'orderby' => 'meta_value_num',
                    'order' => 'ASC'
                )
            );

            // Ta bort samtliga bilder som är i bildspelet
            foreach($images as $image) {
                wp_delete_post($image->ID);
            }

            // Ta bort bildspelet
            wp_delete_post($_GET['post']);
        }
    }

    /**
     * Skriver ut en list puff för bildspelet. Används i listningen
     * @param WP_Post $slider
     */
    function output_slider_puff( $slider = null ) {
        $new = $slider ? "" : "new";
        $id = empty($new) ? "" : "new-slider";
        echo "<li class='puff $new col-sm-12' id='$id'>";
        if ($slider != null) {
            echo "<div class='row'>";
            echo "<div class='col-sm-9'>";
            echo "<h2>$slider->post_title</h2>";
            echo "</div>";
            $edit_url = admin_url("/admin.php?page=timelab_cms_sliders&post={$slider->ID}&action=edit");
            echo "<div class='col-sm-3'>";
            echo "<a href='$edit_url' class='btn btn-lg btn-success btn-block'><span class='glyphicon glyphicon-camera'></span> Redigera bildspel</a>";
            echo "</div>";
            echo "</div>";
        }
        echo "</li>";

    }

    /**
     * Används för att skriva ut bild "puffarna" i bildspels editorn
     * @param int $image_id IDet på bilden som ska skrivas ut, NULL ifall en template ska skrivas ut (används av javascriptet bland annat)
     */
    function output_slider_image($image_id = null) {

        $slider_id = (isset($_GET['post'])) ? $_GET['post'] : null;
        $media_id = ($image_id != null) ? get_post_meta($image_id, 'media_id', true) : '{0}';
        $image_url = ($image_id != null) ? wp_get_attachment_image_src($media_id, array(1024, 1024), false)[0] : '{1}';
        $image_href = ($image_id != null) ? get_post_meta($image_id, 'href', true) : '';
        $image_target = ($image_id != null) ? get_post_meta($image_id, 'target', true) : '';
        $text = ($image_id != null) ? get_post_meta($image_id, 'text', true) : '';
        $title = ($image_id != null) ? get_post($image_id)->post_title : '';
        $sub_title = ($image_id != null) ? get_post_meta($image_id, 'subtitle', true) : '';
        $order = ($image_id != null) ? get_post_meta($image_id, 'order', true) : '{2}';
        $date_from = ($image_id != null) ? get_post_meta($image_id, 'date_from', true) : '';
        $date_to = ($image_id != null) ? get_post_meta($image_id, 'date_to', true) : '';

        $title_input_type = (get_post_meta($slider_id, 'enable_title', true) == 1) ? 'text' : 'hidden';
        $subtitle_input_type = (get_post_meta($slider_id, 'enable_subtitle', true) == 1) ? 'text' : 'hidden';

        echo "
            <li class='slider-image-panel'>
            <div class='panel panel-branded'>
                <div class='panel-heading drag-handle' data-toggle='tooltip' title='Dra och släpp för att sortera'>
                    <span class='glyphicon glyphicon-move'></span>
                </div>

                <div class='panel-body'>
                    <div class='col-sm-7 slider-image'>
                        <span class='glyphicon glyphicon-trash'></span>
                        <img class='img-responsive drag-handle' src='$image_url' />
                        <div class='clear'></div>
                    </div>
                    <div class='col-sm-5'>

                        <div class='form-group col-sm-12 $title_input_type'>
                            <label>Rubrik</label>
                            <input type='$title_input_type' class='form-control input-sm' name='image_title[]' value='$title'></input>
                        </div>

                        <div class='form-group col-sm-12 $subtitle_input_type'>
                            <label>Underubrik</label>
                            <input type='$subtitle_input_type' class='form-control input-sm' name='image_subtitle[]' value='$sub_title'></input>
                        </div>

                        <div class='form-group col-sm-12'>
                            <label>Bildtext</label>
                            <textarea class='form-control input-sm' name='image_text[]'>$text</textarea>
                        </div>
                        <div class='form-group col-sm-8'>
                            <label>Länkadress</label>
                            <input class='form-control input-sm' type='text' name='image_href[]' value='$image_href'></input>
                            <div class='clear'></div>
                        </div>
                        <div class='form-group col-sm-4'>
                            <label>Öppna länk i</label>
                            <select name='image_target[]' data-value='$image_target' class='form-control input-sm'>
                                <option value='_blank'>Ny flik</option>
                                <option value='_self'>Samma flik</option>
                            </select>
                            <div class='clear'></div>
                        </div>
                        <div class='clear'></div>

                        <div class='form-group col-sm-12'>
                            <label>Visas</label>
                            <div class='input-group input-daterange'>
                                <input name='image_date_from[]' type='text' class='input-sm form-control' placeholder='fr.o.m' name='start' value='$date_from' />
                                <span class='input-group-addon'>-</span>
                                <input name='image_date_to[]' type='text' class='input-sm form-control' placeholder='t.o.m' name='end' value='$date_to' />
                            </div>
                            <div class='clear'></div>
                        </div>

                        <input type='hidden' name='media_id[]' value='$media_id'></input>
                        <input type='hidden' name='image_id[]' value='{$image_id}'></input>
                        <input type='hidden' name='image_order[]' value='$order'></input>
                        <input type='hidden' name='image_remove[]' value=''></input>
                        <div class='clear'></div>
                        <div class='col-sm-12'>
                            <a href='#' class='btn btn-sm btn-block btn-danger remove-image'><span class='glyphicon glyphicon-trash'></span> Ta bort</a>
                        </div>
                    </div>
                </div>
            </div></li>";
    }

    /**
     * Hämtar ut alla bilder från en slider, används för att skriva ut slidern på frontend sidor.
     * @param int $id Idet för slidern, lämna blank för att hämta IDet från $_GET['post'] istället
     * @return array Array med alla slides
     */
    public function get_slider_images($id = 0) {

        if ($id == 0) $id = $_GET['post'];

        $title_enabled = get_post_meta($id, 'enable_title', true) == 1 ? true : false;
        $subtitle_enabled = get_post_meta($id, 'enable_subtitle', true) == 1 ? true : false;

        $images = get_posts(
            array(
                'post_type' => 'timelab_cms_image',
                'posts_per_page' => -1,
                'post_parent' => $id,
                'meta_key' => 'order',
                'orderby' => 'meta_value_num',
                'order' => 'ASC'
            )
        );

        $slides = array();

        foreach ($images as $index=>$image) {

            $date_from = strtotime(get_post_meta($image->ID, 'date_from', true));
            $date_to = strtotime(get_post_meta($image->ID, 'date_to', true));
            $date_now = strtotime(date("Y/m/d"));

            if (!empty($date_from) || !empty($date_to)) {
                // Om från eller till inte är satt, sätt då den till 0 respektive oändligt
                $date_from = (!empty($date_from)) ? $date_from : 0;
                $date_to = (!empty($date_to)) ? $date_to : PHP_INT_MAX;

                if (($date_now < $date_from) || ($date_now > $date_to)) {
                    continue;
                }
            }
            
            $slider_image = array();

            $slider_image['text'] = get_post_meta($image->ID, 'text', true);
            $slider_image['href'] = get_post_meta($image->ID, 'href', true);
            $slider_image['target'] = get_post_meta($image->ID, 'target', true);

            $slider_image['title'] = ($title_enabled) ? $image->post_title : null;
            $slider_image['subtitle'] = ($subtitle_enabled) ? get_post_meta($image->ID, 'subtitle', true) : null;

            $media_id = get_post_meta($image->ID, 'media_id', true);
            $slider_image['url'] = wp_get_attachment_image_src($media_id, array(1080, 1080), false)[0];

            $slides[] = (object)$slider_image;
        }



        return $slides;
    }
}
