<?php

namespace Timelab\Cms\Modules;


use Timelab\Cms\ApiInterface;
use Timelab\Cms\ModuleAbstract;

/**
 * Simplifies the Admin interface by stripping unnecessary links from the menu and other stuff.
 *
 * Class Simplify
 * @package Timelab\Cms\Modules
 */
class Simplify extends ModuleAbstract {


    public function __construct()
    {
        parent::__construct();

        // Hooks
        add_action( 'admin_menu', array($this, 'changeAdminMenu'));
        add_action( 'admin_bar_menu', array($this, 'changeAdminBarMenu'), 999 );
    }


    /**
     * @return bool True if menu should only appear for administrators, False if it should appear for everyone
     */
    public function isAdminOnly()
    {
        return true;
    }

    /**
     * Returns an array of javascript filenames that should be included when the module is activated.
     * @return string[] Array of javascript filenames to include
     */
    public function jsFiles()
    {
        return null;
    }

    /**
     * Called on initialization of the Wordpress Admin by hooking into admin_init
     */
    public function onInitialize()
    {

    }

    /**
     * @param $wpAdminBar /WP_Admin_Bar
     */
    public function changeAdminBarMenu($wpAdminBar) {
        foreach ($wpAdminBar->get_nodes() as $node) {
            if ($node->id === 'wp-logo' || $node->id === 'new-content' || $node->id === 'comments') {
                $wpAdminBar->remove_node($node->id);
            }
        }
    }

    /**
     * Removes a bunch of unnecessary menu alternatives
     *
     * @todo Make a admin interface for changing which links get hidden/not-hidden
     */
    public function changeAdminMenu() {
        if (!$this->getCmsInstance()->isSuperAdmin()) {
            remove_menu_page('link-manager.php');
            remove_menu_page('tools.php');
            remove_menu_page('edit-comments.php');
            remove_menu_page('edit.php');
            remove_menu_page('edit.php?post_type=page');
        }
    }

    /**
     * Gets a list of names of all dependant modules
     * @return string[]|null Array with names of all required modules, null if no dependencies
     */
    public function getDependencies()
    {

    }

    /**
     * Gets the menu order of the module
     * @return int
     */
    public function getMenuOrder()
    {
        return 0;
    }

    public function addToMenu()
    {
        return null;
    }

    /**
     * Called when user is routed to this module, this is done during initialization of wordpress
     * (Good if you want to redirect after action is finished)
     * @param $action string|null The action to be executed
     * @param null $id int The ID of the object to perform the action on
     */
    public function earlyExecute($action = null, $id = null)
    {
        // TODO: Implement earlyExecute() method.
    }

    /**
     * Called when user is routed to this module
     * @param $action string|null The action to be executed
     * @param null $id int The ID of the object to perform the action on
     */
    public function execute($action = null, $id = null)
    {
        // TODO: Implement execute() method.
    }

    /**
     * Returns the API interface of the module
     * @return ApiInterface
     */
    public function getApi()
    {
        // TODO: Implement getApi() method.
    }

} 