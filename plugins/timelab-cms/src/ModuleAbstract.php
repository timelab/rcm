<?php

namespace Timelab\Cms;


use Timelab\Cms\Objects\Setting;

abstract class ModuleAbstract implements ModuleInterface {

    /** @var Setting[] $settings */
    private $settings = array();

    public function __construct() {
        add_action('admin_init', array($this, 'onInitialize'));
        add_action('admin_init', array($this, 'earlyRoute'));
        add_action('admin_menu', array($this, 'addToMenu'));
        add_action('admin_enqueue_scripts', array($this, 'includeFiles'));
    }

    /**
     * Gets the name of the admin page, used when routing
     * @return string
     */
    public function getName()
    {
        return str_replace(Cms::MODULE_NAMESPACE, '', get_class($this));
    }

    /**
     * Gets the menu title.
     * @return string
     */
    public function getMenuTitle()
    {
        return $this->getName();
    }

    /**
     * Gets the menu icon.
     * @return string
     */
    public function getMenuIcon()
    {
        return "dashicons-list-view";
    }


    /**
     * @return bool True if menu should only appear for administrators, False if it should appear for everyone
     */
    abstract public function isAdminOnly();

    /**
     * Returns an array of javascript filenames that should be included when the module is activated.
     * @return string[] Array of javascript filenames to include
     */
    abstract public function jsFiles();


    public function includeFiles() {
        if(preg_match('/timelab_cms/', get_current_screen()->base)){
            if ($this->jsFiles() !== null) {
                foreach ($this->jsFiles() as $file) {
                    wp_register_script($file, plugins_url() . "/timelab-cms/assets/js/{$file}", array('jquery-ui-sortable'));
                    wp_enqueue_script($file);
                }
            }
            wp_register_script('default-google-admin', 'https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyBZSh82BOfJHnzFTkhsO44bslos5wV8Wqg&sensor=false');
            wp_enqueue_script('default-google-admin');
        }
    }


    /**
     * Gets the current CMS instance
     * @return Cms The CMS instance
     * @throws \Exception If CMS instance is not set
     */
    public function getCmsInstance() {
        if (!isset(Cms::$instance)) {
            throw new \Exception('CMS instance not set.');
        }

        return Cms::$instance;
    }

    /**
     * Gets the module name + route prefix
     * @return string The full page name
     */
    public function getFullName() {
        return Cms::ROUTE_NAME_PREFIX . $this->getName();
    }

    /**
     * Registers a setting, the name of the setting in the DB becomes 'timelab_cms_{ModuleName}_{SettingName}, so you
     * only have to make sure the name is unique for this module, not the entire CMS
     * @param $name string The name of the setting
     * @param $title string Display title of the setting
     * @param $description string The description of the setting
     * @param $type string The type of setting to use
     */
    public function registerSetting($name, $title, $description, $type) {
        $this->settings[] = new Setting(Cms::SETTING_NAME_PREFIX . $this->getName() . "_" . $name, $title, $description, $type);
    }

    /**
     * Gets a list of settings
     * @return \Timelab\Cms\Objects\Setting[] List of all the settings associated with this module
     */
    public function getSettings()
    {
        return $this->settings;
    }

    /**
     * Gets the specified setting.
     * @param $name string The name of the setting
     * @return \Timelab\Cms\Objects\Setting|null
     */
    public function getSetting($name)
    {
        foreach ($this->settings as $setting) {
            if ($setting->getName() === Cms::SETTING_NAME_PREFIX . $this->getName() . "_" . $name) {
                return $setting;
            }
        }

        return null;
    }

    /**
     * Gets the url to the module admin
     * @param null $action Optionally you can specify which action you want the url to point to
     * @param null $id int The ID of the object to perform the action on
     * @return string The full url
     */
    public function getUrl($action = null, $id = null) {
        $action = ($action !== null) ? "&action=$action" : "";
        $id = ($id !== null) ? "&id=$id" : "";
        return admin_url("admin.php?page=" . $this->getFullName() . $action . $id);
    }

    /**
     * Called on initialization of the Wordpress Admin by hooking into admin_init
     */
    abstract public function onInitialize();

    /**
     * Adds the module to the Wordpress menu and links it to the route() function of the module
     */
    public function addToMenu() {
        $capability = ($this->isAdminOnly()) ? 'manage_options' : 'edit_pages';

        add_menu_page( $this->getMenuTitle(), $this->getMenuTitle(), $capability, $this->getFullName(), array($this, 'route'), $this->getMenuIcon(), $this->getMenuOrder() );
    }

    /**
     * Executes the execute() method and passes it the current action parameter
     */
    public function route() {
        $action = (isset($_GET['action'])) ? $_GET['action'] : null;
        $id     = (isset($_GET['id'])) ? intval($_GET['id']) : null;
        $this->execute($action, $id);
    }

    /**
     * Executes the earlyExecute() method and passes it the current action parameter
     */
    public function earlyRoute() {
        if (!isset($_GET['page']) || $_GET['page'] !== $this->getFullName()) {
            return;
        }

        add_filter('admin_body_class', array($this, "addBodyClass"));
        $action = (isset($_GET['action'])) ? $_GET['action'] : null;
        $id     = (isset($_GET['id'])) ? intval($_GET['id']) : null;
        $this->earlyExecute($action, $id);
    }

    public function addBodyClass($classes) {
        return $classes . " timelab-cms";
    }
}