<?php
namespace Timelab\Cms;

use Timelab\Cms\Modules;

require_once('ModuleInterface.php');
require_once('ModuleAbstract.php');
require_once('ApiInterface.php');
require_once('ApiAbstract.php');
require_once('ObjectInterface.php');
require_once('DatabaseObjectAbstract.php');
require_once('Objects/Setting.php');

/**
 * The main class of the CMS, It sets up the CMS and loads the modules
 * Class Cms
 * @package Timelab\Cms
 */
class Cms {
    /**
     * The CMS instance.
     * @var Cms
     */
    public static $instance;

    const WP_OPTIONS_GROUP_NAME = 'timelab_cms';

    const MODULE_FOLDER = '/Modules/';
    const MODULE_NAMESPACE = 'Timelab\Cms\Modules\\';

    const OBJECT_FOLDER = '/Objects/';
    const OBJECT_NAMESPACE = 'Timelab\Cms\Objects\\';

    const ROUTE_NAME_PREFIX = 'timelab_cms_';
    const SETTING_NAME_PREFIX = 'timelab_';

    private $twig;

    /**
     * Array containing all the loaded modules
     * @var ModuleInterface[] Array with all loaded modules
     */
    private $loadedModules = array();

    /**
     * Constructor for the CMS
     */
    public function __construct() {
        Cms::$instance = &$this;

        $modules = array('Options');

        // Get the modules that are configured to run.
        $modulesToLoad = get_option('timelab_Options_running_modules');

        if (!empty($modulesToLoad)) {
            $modulesToLoad = explode(',', $modulesToLoad);

            foreach ($modulesToLoad as $module) {
                $modules[] = $module;
            }
        }

        // Load Twig
        $this->loadTwig();

        // Always load Options module
        $this->loadModule('Options');

        foreach ($modules as $module) {
            $this->loadModule($module);
        }

        if (is_admin()) {
            add_action('activated_plugin', array($this, 'setPluginOrder'));
            add_action('save_post', array($this, 'clearCache'));
        }

    }

    /**
     * Loads twig (if inside the admin)
     */
    private function loadTwig()
    {
        if (is_admin()) {
            require_once('Vendor/autoload.php');

            $loader = new \Twig\Loader\FilesystemLoader(dirname(__FILE__) . '/Templates');
            $this->twig = new \Twig\Environment($loader, array());
        }
    }

    /**
     * Loads a module.
     * @param $moduleName string The name of the module
     * @return ModuleInterface|bool The module if successful, otherwise false
     * @throws \Exception if module is not found
     */
    private function loadModule($moduleName) {

        // Make sure module isn't already loaded
        foreach ($this->loadedModules as $module) {
            if ($module->getName() === $moduleName) {
                return $module;
            }
        }

        $modulePath  = dirname(__FILE__) . self::MODULE_FOLDER . $moduleName . '.php';
        $moduleClassName = self::MODULE_NAMESPACE . $moduleName;

        if (realpath($modulePath)) {
            require_once($modulePath);

            // Create module and append it to loaded modules
            /** @var ModuleInterface $module */
            $module = new $moduleClassName();
            $this->loadedModules[] = $module;

            // Load all dependencies
            if ($module->getDependencies() !== null) {
                foreach ($module->getDependencies() as $dependency) {
                    $this->loadModule($dependency);
                }
            }

            // Return the module
            return $module;
        }

        throw new \Exception("Could not find the specified module: $moduleName");
    }

    /**
     * @param $templateName string Name of the template
     * @param array $data
     * @return string The rendered HTML
     */
    public function render($templateName, $data = array()) {
        $template = $this->twig->load($templateName);

        $data = array_merge($data,
            array(
                'layout_title' => get_admin_page_title(),
                'layout_slug' => get_current_screen()->parent_base,
                'is_admin' => $this->isSuperAdmin()
            )
        );

        return $template->render($data);
    }

    /**
     * Returns the specified module if loaded
     * @param $name string The name of the module to load
     * @return ModuleInterface|null The module if it is loaded, `null` if not.
     */
    public function getModule($name) {
        foreach ($this->loadedModules as $module) {
            if ($module->getName() === $name) {
                return $module;
            }
        }

        return null;
    }

    /**
     * Gets the API of the specified module.
     * @param $moduleName
     * @return ApiInterface|null Returns the specified modules Api, `null` if Module or Api couldn't be found.
     */
    public function getApi($moduleName) {
        $module = $this->getModule($moduleName);

        if (!empty($module)) {
            return $this->getModule($moduleName)->getApi();
        }

        return null;
    }

    /**
     * Gets all the loaded modules
     * @return ModuleInterface[] All loaded modules
     */
    public function getModules() {
        return $this->loadedModules;
    }

    /**
     * Gets all the available module classes reflection (Loaded or not)
     * @return \ReflectionClass[] A list of all modules
     */
    public function getAvailableModules() {
        $modules = array();

        foreach (glob(dirname(__FILE__) . self::MODULE_FOLDER . '*.php') as $file) {
            try {
                require_once($file);
                $module = new \ReflectionClass(self::MODULE_NAMESPACE . basename($file, '.php'));
                if ($module->implementsInterface(__NAMESPACE__ . '\ModuleInterface')) {
                    $modules[] = $module;
                }

            } catch (\Exception $e) {

            }
        }

        return $modules;
    }

    /**
     * Gets all the available Object types classes reflection
     * @return \ReflectionClass[] A list of all Object Types
     */
    public function getAvailableObjects() {
        $objects = array();

        foreach (glob(dirname(__FILE__) . self::OBJECT_FOLDER . '*.php') as $file) {
            try {
                require_once($file);
                $object = new \ReflectionClass(self::OBJECT_NAMESPACE . basename($file, '.php'));
                if ($object->implementsInterface(__NAMESPACE__ . '\ObjectInterface')) {
                    $objects[] = $object;
                }
            } catch (\Exception $e) {

            }
        }

        return $objects;
    }

    /**
     * Makes sure the Timelab CMS is loaded last.
     */
    public function setPluginOrder() {
        $path = 'timelab-cms/timelab-cms.php';

        if ($plugins = get_option('active_plugins')) {
            if ($key = array_search($path, $plugins)) {
                array_splice($plugins, $key, 1);
                array_push($plugins, $path);

                update_option('active_plugins', $plugins);
            }
        }
    }

    /**
     * Clears W3 Total Cache
     */
    public function clearCache() {
        if (function_exists('w3tc_pgcache_flush')) {
            w3tc_pgcache_flush();
        }
    }

    /**
     * Returns if the user has administration privileges
     * @return bool
     */
    public static function isSuperAdmin() {
        return current_user_can('manage_options');
    }

    public static function canUserEdit() {
        return current_user_can('edit_posts');
    }
}